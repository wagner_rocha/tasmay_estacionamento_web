/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.service;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.EstacionamentoException;
import br.pucrs.psa.negocio.interfaces.CancelaFachadaLocal;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Tasmay
 */
@WebService(serviceName = "CancelaService")
public class CancelaService {
    @EJB
    private CancelaFachadaLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "emiteTicket")
    public Ticket emiteTicket(@WebParam(name = "placa") String placa, @WebParam(name = "palavraChave") String palavraChave) throws EstacionamentoException {
        return ejbRef.emiteTicket(placa, palavraChave);
    }

    @WebMethod(operationName = "saidaTicket")
    public Ticket saidaTicket(@WebParam(name = "codigo") int codigo) throws EstacionamentoException {
        return ejbRef.saidaTicket(codigo);
    }

    @WebMethod(operationName = "validaTicket")
    public Boolean validaTicket(@WebParam(name = "codigo") int codigo) throws EstacionamentoException {
        return ejbRef.validaTicket(codigo);
    }
    
}
