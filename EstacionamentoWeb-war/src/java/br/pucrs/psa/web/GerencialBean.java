/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrs.psa.web;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.EstacionamentoException;
import br.pucrs.psa.negocio.interfaces.GerencialFachadaLocal;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Tasmay
 */
@Named(value = "gerencialBean")
@SessionScoped
public class GerencialBean implements Serializable {

    @EJB
    private GerencialFachadaLocal gerencialEJB;

    private Ticket ticket;
    private int codigo;
    private int mes;
    private int ano;
    private int totalPagamento;
        

    public void ticketslPagos() {
        try {
            totalPagamento = gerencialEJB.ticketsPagos(mes, ano);
            JsfUtil.addSuccessMessage("Total de Tickets Pagos:" + totalPagamento);
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
    }
    
        public void ticketslLiberados() {
        try {
            totalPagamento = gerencialEJB.ticketsLiberados(mes, ano);
            JsfUtil.addSuccessMessage("Total de Tickets Liberados:" + totalPagamento);
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
    }
        
                public void totalRecebido() {
        try {
            String valor = gerencialEJB.valoTotalRecebido(mes, ano);
            JsfUtil.addSuccessMessage("Valor Total Recebido:" + valor);
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public double getTotalPagamento() {
        return totalPagamento;
    }

    public void setTotalPagamento(int totalPagamento) {
        this.totalPagamento = totalPagamento;
    }

    
    
}
