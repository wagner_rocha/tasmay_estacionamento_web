/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrs.psa.web;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.EstacionamentoException;
import br.pucrs.psa.negocio.interfaces.CaixaFachadaLocal;
import br.pucrs.psa.negocio.interfaces.CancelaFachadaLocal;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Tasmay
 */
@Named(value = "ticketBean")
@SessionScoped
public class TicketBean implements Serializable {

    @EJB
    private CaixaFachadaLocal caixaEJB;

    @EJB
    private CancelaFachadaLocal cancelaEJB;

    private Ticket ticket;
    private String placa;
    private int codigo;
    private String palavraChave;
    private String palavraChaveGerada;
    private StreamedContent barcode;

    public String create() {
        try {
            ticket = cancelaEJB.emiteTicket(placa, palavraChave);

            JsfUtil.addSuccessMessage("Ticket Emitido!");
            return "View";
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
        return "Create";
    }

    public String saidaTicket() {
        try {
            ticket = cancelaEJB.saidaTicket(codigo);

            JsfUtil.addSuccessMessage("Ticket Liberado!");
            return "SaidaView";
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
        return "Saida";
    }

    public String consultaTicket() {
        try {
            ticket = caixaEJB.consultaTicket(codigo);
            return "SaidaView";
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
        return "Consulta";
    }
    
        public String pagamento() {
        try {
            ticket = caixaEJB.pagamento(codigo);
            return "SaidaView";
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
        return "Pagamento";
    }
        
    public String liberar() {
        try {
            ticket = caixaEJB.liberacaoTicket(codigo, false);
            return "SaidaView";
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
        return "Pagamento";
    }

    public void calcularPagamento() {
        try {
            Double valor = caixaEJB.calculaPagamento(codigo);
            JsfUtil.addSuccessMessage("Valor a ser pago: R$" + valor);
        } catch (EstacionamentoException e) {
            JsfUtil.addErrorMessage(e.getMessage());
        }
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPalavraChaveGerada() {
        palavraChaveGerada = cancelaEJB.geraPalavraChave();
        return palavraChaveGerada;
    }

    public String getPalavraChave() {
        return palavraChave;
    }

    public void setPalavraChave(String palavraChave) {
        this.palavraChave = palavraChave;
    }

    public StreamedContent getBarcode() {
        geraCodigoBarra("1110");
        return barcode;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    private void geraCodigoBarra(String codigo) {
        try {

            //Barcode
            File barcodeFile = new File("dynamicbarcode");
            BarcodeImageHandler.saveJPEG(BarcodeFactory.createCode39(codigo, true), barcodeFile);
            barcode = new DefaultStreamedContent(new FileInputStream(barcodeFile), "image/jpeg");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
