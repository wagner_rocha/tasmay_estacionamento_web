/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrs.psa.web;

import br.pucrs.psa.negocio.interfaces.FuncionarioFachadaLocal;
import br.pucrs.psa.entidades.FuncionarioDTO;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Tasmay
 */
@Named(value = "funcionarioBean")
@SessionScoped
public class FuncionarioBean implements Serializable {

    @EJB
    private FuncionarioFachadaLocal fachadaEJB;
    
    private FuncionarioDTO current;

    private List<FuncionarioDTO> funcionarios;

    public List<FuncionarioDTO> getFuncionarios() {
        funcionarios = fachadaEJB.getFuncionarios();
        return funcionarios;
    }

    public FuncionarioDTO getSelected() {
        if (current == null) {
            current = new FuncionarioDTO();
        }
        return current;
    }
    
    public String create() {
        try {
            fachadaEJB.cadastrarFuncionario(current.getNome());
            JsfUtil.addSuccessMessage("Funcionario Cadastrado!");
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Erro ao cadastrar Funcionario");
            return null;
        }
    }

}
