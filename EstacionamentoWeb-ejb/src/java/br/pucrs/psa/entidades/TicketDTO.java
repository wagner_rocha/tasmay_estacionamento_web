/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Wagner
 */

public class TicketDTO implements Serializable {
    
    private Integer id;
    private int codigo;
    private Date dataentrada;
    private Date datasaida;
    private BigDecimal valorpago;
    private String statuspgto;
    private String statusliberado;
    private Funcionario codfunc;

    public TicketDTO() {
    }

    public TicketDTO(Integer id) {
        this.id = id;
    }

    public TicketDTO(Integer id, int codigo, Date dataentrada, BigDecimal valorpago, String statuspgto, String statusliberado) {
        this.id = id;
        this.codigo = codigo;
        this.dataentrada = dataentrada;
        this.valorpago = valorpago;
        this.statuspgto = statuspgto;
        this.statusliberado = statusliberado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getDataentrada() {
        return dataentrada;
    }

    public void setDataentrada(Date dataentrada) {
        this.dataentrada = dataentrada;
    }

    public Date getDatasaida() {
        return datasaida;
    }

    public void setDatasaida(Date datasaida) {
        this.datasaida = datasaida;
    }

    public BigDecimal getValorpago() {
        return valorpago;
    }

    public void setValorpago(BigDecimal valorpago) {
        this.valorpago = valorpago;
    }

    public String getStatuspgto() {
        return statuspgto;
    }

    public void setStatuspgto(String statuspgto) {
        this.statuspgto = statuspgto;
    }

    public String getStatusliberado() {
        return statusliberado;
    }

    public void setStatusliberado(String statusliberado) {
        this.statusliberado = statusliberado;
    }

    public Funcionario getCodfunc() {
        return codfunc;
    }

    public void setCodfunc(Funcionario codfunc) {
        this.codfunc = codfunc;
    }


    @Override
    public String toString() {
        return "Cod: " + codigo;
    }
    
}
