/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tasmay
 */
@Entity
@Table(name = "TICKETS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ticket.findAll", query = "SELECT t FROM Ticket t"),
    @NamedQuery(name = "Ticket.findById", query = "SELECT t FROM Ticket t WHERE t.id = :id"),
    @NamedQuery(name = "Ticket.findByCodigo", query = "SELECT t FROM Ticket t WHERE t.codigo = :codigo"),
    @NamedQuery(name = "Ticket.findByDataentrada", query = "SELECT t FROM Ticket t WHERE t.dataentrada = :dataentrada"),
    @NamedQuery(name = "Ticket.findByDatasaida", query = "SELECT t FROM Ticket t WHERE t.datasaida = :datasaida"),
    @NamedQuery(name = "Ticket.findByValorpago", query = "SELECT t FROM Ticket t WHERE t.valorpago = :valorpago"),
    @NamedQuery(name = "Ticket.findByStatuspgto", query = "SELECT t FROM Ticket t WHERE t.statuspgto = :statuspgto"),
    @NamedQuery(name = "Ticket.findByStatusliberado", query = "SELECT t FROM Ticket t WHERE t.statusliberado = :statusliberado"),
    @NamedQuery(name = "Ticket.findMAxID", query = "SELECT max(t.id) as maxid from Ticket t"),
    @NamedQuery(name = "Ticket.TotalRecebido", query = "SELECT sum(t.valorpago) from Ticket t where EXTRACT(MONTH FROM t.dataentrada) = :mes and EXTRACT(YEAR FROM t.dataentrada) = :ano"),
    @NamedQuery(name = "Ticket.TicketsPagos", query = "SELECT count(t) from Ticket t where EXTRACT(MONTH From t.dataentrada) = :mes and EXTRACT(YEAR FROM t.dataentrada) = :ano and t.statuspgto = 'S'"),
    @NamedQuery(name = "Ticket.TicketsLiberados", query = "SELECT count(t) from Ticket t where EXTRACT(MONTH FROM t.dataentrada) = :mes and EXTRACT(YEAR FROM t.dataentrada) = :ano and t.statuspgto = 'N' and t.statusliberado = 'S'")})

public class Ticket implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    
    @NotNull    
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private int codigo;
    
    @NotNull
    @Basic(optional = false)
    @Column(name = "DATAENTRADA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataentrada;
    
    @Column(name = "DATASAIDA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datasaida;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @Column(name = "VALORPAGO")
    private double valorpago;
    
    
    @Size(min = 1, max = 1)
    @Column(name = "STATUSPGTO")
    private String statuspgto;
    
    
    @Size(min = 1, max = 1)
    @Column(name = "STATUSLIBERADO")
    private String statusliberado;
    
    @JoinColumn(name = "CODFUNC", referencedColumnName = "CODFUNC")
    @ManyToOne
    private Funcionario codfunc;
    
    @NotNull
    @Basic(optional = false)
    @Size(min = 1, max = 7)
    @Column(name = "PLACA")
    private String placa;
    
    @NotNull
    @Basic(optional = false)
    @Size(min = 1, max = 5)
    @Column(name = "PALAVRACHAVE")
    private String palavraChave;

    public Ticket() {
        setStatuspgto("N");
        setValorpago(0);
        setStatusliberado("N");
    }

    public Ticket(Integer id) {
        super();
        this.id = id;
    }

    public Ticket(Integer id, int codigo, Date dataentrada, double valorpago, String statuspgto, String statusliberado) {
        this.id = id;
        this.codigo = codigo;
        this.dataentrada = dataentrada;
        this.valorpago = valorpago;
        this.statuspgto = statuspgto;
        this.statusliberado = statusliberado;
    }

    public Ticket(int codigo, Date dataentrada, String placa, String palavraChave) {
        super();
        this.codigo = codigo;
        this.dataentrada = dataentrada;
        this.placa = placa;
        this.palavraChave = palavraChave;
    }
         

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPalavraChave() {
        return palavraChave;
    }

    public void setPalavraChave(String palavraChave) {
        this.palavraChave = palavraChave;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getDataentrada() {
        return dataentrada;
    }

    public void setDataentrada(Date dataentrada) {
        this.dataentrada = dataentrada;
    }

    public Date getDatasaida() {
        return datasaida;
    }

    public void setDatasaida(Date datasaida) {
        this.datasaida = datasaida;
    }

    public double getValorpago() {
        return valorpago;
    }

    public void setValorpago(double valorpago) {
        this.valorpago = valorpago;
    }

    public String getStatuspgto() {
        return statuspgto;
    }

    public void setStatuspgto(String statuspgto) {
        this.statuspgto = statuspgto;
    }

    public String getStatusliberado() {
        return statusliberado;
    }

    public void setStatusliberado(String statusliberado) {
        this.statusliberado = statusliberado;
    }

    public Funcionario getCodfunc() {
        return codfunc;
    }

    public void setCodfunc(Funcionario codfunc) {
        this.codfunc = codfunc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ticket)) {
            return false;
        }
        Ticket other = (Ticket) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.pucrs.psa.Entidades.Ticket[ id=" + id + " ]";
    }
    
}
