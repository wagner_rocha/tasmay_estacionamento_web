/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.entidades;

import br.pucrs.psa.entidades.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Juliana
 */

public class FuncionarioDTO implements Serializable {
    
    private Integer codfunc; 
    private String nome;
    private List<Ticket> ticketList;

    public FuncionarioDTO() {
    }

    public FuncionarioDTO(Integer codfunc) {
        this.codfunc = codfunc;
    }

    public FuncionarioDTO(Integer codfunc, String nome) {
        this.codfunc = codfunc;
        this.nome = nome;
    }

    public Integer getCodfunc() {
        return codfunc;
    }

    public void setCodfunc(Integer codfunc) {
        this.codfunc = codfunc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    @Override
    public String toString() {
        return nome;
    }

}
