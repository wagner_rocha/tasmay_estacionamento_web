/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.persistencia;

import br.pucrs.psa.entidades.Ticket;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;



@Stateless
public class TicketFacade extends AbstractFacade<Ticket> implements TicketFacadeLocal {
    @PersistenceContext(unitName = "EstacionamentoWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TicketFacade() {
        super(Ticket.class);
    }
    
    @Override
      public Integer getMaxId() {
        Integer maxId = (Integer) em.createNamedQuery("Ticket.findMAxID").getSingleResult();
        if (maxId == null)
            return 1;
        return maxId + 1;
    }

    @Override
    public long getTicketsPagos(int mes, int ano) {
        
        return (long) em.createNamedQuery("Ticket.TicketsPagos")
                .setParameter("mes", mes)
                .setParameter("ano", ano)
                .getSingleResult();
        
    }

    @Override
    public long getTicketsLiberados(int mes, int ano) {
        return (long) em.createNamedQuery("Ticket.TicketsLiberados")
                .setParameter("mes", mes)
                .setParameter("ano", ano)
                .getSingleResult();
        
    }

    @Override
    public double getTotalRecebido(int mes, int ano) {
        return (Double) em.createNamedQuery("Ticket.TotalRecebido")
                .setParameter("mes", mes)
                .setParameter("ano", ano)
                .getSingleResult();
    }
      
     

      
    
}
