/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.negocio;

import br.pucrs.psa.entidades.FuncionarioDTO;
import br.pucrs.psa.negocio.interfaces.FuncionarioFachadaLocal;
import br.pucrs.psa.entidades.Funcionario;
import br.pucrs.psa.persistencia.FuncionarioFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class FuncionarioFachada implements FuncionarioFachadaLocal {
    @EJB
    private FuncionarioFacadeLocal funcionarioEJB;

    @Override
    public void cadastrarFuncionario(String nome) throws EstacionamentoException {
        
        Funcionario f = new Funcionario();
        f.setNome(nome);
        funcionarioEJB.create(f);    
    }
    
    @Override
    public List<FuncionarioDTO> getFuncionarios() {
        try {
            List<Funcionario> funcionarios = funcionarioEJB.findAll();
            return copiarParaDTO(funcionarios);
        } catch (Exception e) {
            throw new EstacionamentoException("Erro ao buscar", e);
        }
    }
    
        
        private List<FuncionarioDTO> copiarParaDTO(List<Funcionario> todos) {
        List<FuncionarioDTO> dtos = new ArrayList<FuncionarioDTO>(todos.size());
        for (Funcionario f : todos) {
            Integer id = f.getCodfunc();
            String nome = f.getNome();
            FuncionarioDTO dto = new FuncionarioDTO(id, nome);
            dtos.add(dto);
        }
        return dtos;
    }

   
    
}
