/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrs.psa.negocio;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.interfaces.CaixaFachadaLocal;
import br.pucrs.psa.persistencia.TicketFacadeLocal;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class CaixaFachada implements CaixaFachadaLocal {

    @EJB
    private TicketFacadeLocal ticketEJB;

    @Override
    public Double calculaPagamento(int codigo) throws EstacionamentoException {

        try {
            Ticket tk = consultaTicket(codigo);

            Calendar dataSaida = Calendar.getInstance();

            if (tk.getDatasaida() == null) {
                dataSaida.setTime(new Date());
            } else {
                dataSaida.setTime(tk.getDatasaida());
            }

            Calendar dataEntrada = Calendar.getInstance();
            dataEntrada.setTime(tk.getDataentrada());

            int diaEntrada = dataEntrada.get(Calendar.DAY_OF_MONTH);
            int diaSaida = dataSaida.get(Calendar.DAY_OF_MONTH);

            Calendar horaFechamento = Calendar.getInstance();
            horaFechamento.setTime(dataSaida.getTime());
            int ano = horaFechamento.get(Calendar.YEAR);
            int mes = horaFechamento.get(Calendar.MONTH);
            int dia = horaFechamento.get(Calendar.DATE);
            horaFechamento.set(ano, mes, dia, 2, 0);

            long tempo = ((dataSaida.getTime().getTime() / 60000) - (dataEntrada.getTime().getTime() / 60000));

            if (diaEntrada < diaSaida && dataSaida.after(horaFechamento)) {
                return 50.0;
            } else if (tempo <= 15) {
                return 0.0;
            } else {
                return 10.0;
            }
        } catch (EstacionamentoException ex) {
            throw new EstacionamentoException(ex.getMessage());
        }
    }

    @Override
    public Ticket liberacaoTicket(int codigo, boolean pagamento) throws EstacionamentoException {

        try {
            Ticket tk = consultaTicket(codigo);

            if (tk.getStatusliberado().equals("S")) {
                throw new EstacionamentoException("Ticket já foi liberado");
            }

            if (!pagamento) {
                return liberaTicket(codigo);
            } else {
                double valor = calculaPagamento(codigo);
                tk.setValorpago(valor);
                tk.setStatuspgto("S");
                tk.setStatusliberado("S");
                ticketEJB.edit(tk);
                return tk;
            }
        } catch (EstacionamentoException ex) {
            throw new EstacionamentoException(ex.getMessage());
        }

    }

    private Ticket liberaTicket(int codigo) {
        Ticket tk = consultaTicket(codigo);
        tk.setStatusliberado("S");
        ticketEJB.edit(tk);
        return tk;
    }

    @Override
    public Ticket consultaTicket(int codigo) throws EstacionamentoException {
        try {
            Ticket tk = ticketEJB.find(codigo);
            if (tk == null) {
                throw new EstacionamentoException("Ticket não Encontrado");
            } else {
                return tk;
            }

        } catch (EstacionamentoException e) {
            throw new EstacionamentoException(e.getMessage());
        }
    }

    @Override
    public Ticket pagamento(int codigo) throws EstacionamentoException {
        try {
            Ticket tk = ticketEJB.find(codigo);

            if (tk == null) {
                throw new EstacionamentoException("Ticket não Encontrado");
            }
            
            if (tk.getStatuspgto().equals("S")) {
                throw new EstacionamentoException("Ticket já foi Pago.");
            }

            double valor = calculaPagamento(codigo);
            tk.setValorpago(valor);
            tk.setStatuspgto("S");
            tk.setStatusliberado("S");
            ticketEJB.edit(tk);
            return tk;
        } catch (EstacionamentoException e) {
            throw new EstacionamentoException(e.getMessage());
        }
    }

}
