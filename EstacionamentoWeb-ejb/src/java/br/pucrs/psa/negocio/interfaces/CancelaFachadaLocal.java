/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.negocio.interfaces;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.EstacionamentoException;
import javax.ejb.Local;


@Local
public interface CancelaFachadaLocal {

    Ticket emiteTicket(String placa, String palavraChave) throws EstacionamentoException;

    Ticket saidaTicket(int codigo) throws EstacionamentoException;

    Boolean validaTicket(final int codigo) throws EstacionamentoException;

    String geraPalavraChave() throws EstacionamentoException;
    
}
