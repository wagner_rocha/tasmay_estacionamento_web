/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.negocio.interfaces;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.EstacionamentoException;
import javax.ejb.Local;


@Local
public interface CaixaFachadaLocal {

    Double calculaPagamento(int codigo) throws EstacionamentoException;

    Ticket liberacaoTicket(int codigo, boolean pagamento) throws EstacionamentoException;
    Ticket consultaTicket(int codigo) throws EstacionamentoException;

    Ticket pagamento(int codigo) throws EstacionamentoException;
    
}
