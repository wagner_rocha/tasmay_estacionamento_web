/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.negocio.interfaces;

import br.pucrs.psa.negocio.EstacionamentoException;
import br.pucrs.psa.entidades.FuncionarioDTO;
import java.util.List;
import javax.ejb.Local;


@Local
public interface FuncionarioFachadaLocal {

    void cadastrarFuncionario(String nome) throws EstacionamentoException;

    List<FuncionarioDTO> getFuncionarios();
    
}
