/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.negocio.interfaces;

import br.pucrs.psa.negocio.EstacionamentoException;
import javax.ejb.Local;


@Local
public interface GerencialFachadaLocal {

    String valoTotalRecebido(int dia, int ano) throws EstacionamentoException;

    int ticketsLiberados(int dia, int ano) throws EstacionamentoException;

    int ticketsPagos(int dia, int ano) throws EstacionamentoException;
    
}
