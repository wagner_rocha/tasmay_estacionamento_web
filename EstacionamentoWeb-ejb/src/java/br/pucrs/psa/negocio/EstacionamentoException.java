/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrs.psa.negocio;

import javax.ejb.EJBException;


public class EstacionamentoException extends EJBException {

    public EstacionamentoException() {
    }

    public EstacionamentoException(String message) {
        super(message);
    }

    public EstacionamentoException(String message, Exception ex) {
        super(message, ex);
    }
}
