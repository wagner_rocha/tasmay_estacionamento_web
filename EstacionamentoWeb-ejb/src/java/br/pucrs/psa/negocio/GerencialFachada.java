/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.pucrs.psa.negocio;

import br.pucrs.psa.negocio.interfaces.GerencialFachadaLocal;
import br.pucrs.psa.persistencia.TicketFacadeLocal;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class GerencialFachada implements GerencialFachadaLocal {
    @EJB
    private TicketFacadeLocal ticketEJB;

    @Override
    public String valoTotalRecebido(int mes, int ano) throws EstacionamentoException {
        Double valor = ticketEJB.getTotalRecebido(mes, ano);
        return "R$ " + valor;
    }

    @Override
    public int ticketsPagos(int mes, int ano) throws EstacionamentoException {
        long tp = ticketEJB.getTicketsPagos(mes, ano);
        
        return (int) tp;
    }
    
    @Override
    public int ticketsLiberados(int mes, int ano) throws EstacionamentoException {
        long tp = ticketEJB.getTicketsLiberados(mes, ano);
        return (int) tp;
        
    }
   
}
