/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrs.psa.negocio;

import br.pucrs.psa.entidades.Ticket;
import br.pucrs.psa.negocio.interfaces.CancelaFachadaLocal;
import br.pucrs.psa.persistencia.TicketFacadeLocal;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class CancelaFachada implements CancelaFachadaLocal {

    @EJB
    private TicketFacadeLocal ticketEJB;
    private String palavraChave;

    @Override
    public Ticket emiteTicket(String placa, String palavra) throws EstacionamentoException {

        if (placa.length() != 7 || palavra.length() != 5) {
            throw new EstacionamentoException("Valores inválidos");
        }

        if (!Validaplaca(placa)) {
            throw new EstacionamentoException("Placa Inválida");
        }

        if (!palavraChave.equals(palavra)) {
            throw new EstacionamentoException("Palavra chave inválida");
        }

        try {
            Date dataEntrada = new Date();
            int codigo = ticketEJB.getMaxId();
            Ticket tk = new Ticket(codigo, dataEntrada, placa, palavraChave);
            tk.setStatuspgto("N");
            tk.setStatusliberado("N");
            tk.setValorpago(0);
            ticketEJB.create(tk);
            return tk;
        } catch (Exception ex) {
            throw new EstacionamentoException("Falha na emissão do ticket " + ex.getMessage());
        }

    }

    @Override
    public Ticket saidaTicket(int codigo) throws EstacionamentoException {
        try {
            Ticket tk = ticketEJB.find(codigo);
            
            if(tk == null){
                throw new EstacionamentoException("Ticket não encontrado");
            }
            
            if (tk.getDatasaida() != null) {
                throw new EstacionamentoException("Saída já efetuada.");
            }
            if (tk.getStatusliberado().equals("S")) {
                tk.setDatasaida(new Date());
                ticketEJB.edit(tk);
            } else {
                throw new EstacionamentoException("Ticket não Liberado");
            }
            return tk;
        } catch (EstacionamentoException e) {
            throw new EstacionamentoException(e.getMessage());

        }
    }

    @Override
    public Boolean validaTicket(final int codigo) throws EstacionamentoException {
        try {
            Ticket tk = ticketEJB.find(codigo);
            return tk.getStatusliberado().equals("S");
        } catch (Exception ex) {
            throw new EstacionamentoException("Falha na busca. " + ex.getMessage());
        }
    }

    @Override
    public String geraPalavraChave() throws EstacionamentoException {
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        palavraChave = sb.toString();
        return palavraChave;

    }

    public String getPalavraChave() {
        return palavraChave;
    }

    private boolean Validaplaca(String placa) {
        Pattern pattern = Pattern.compile("^[A-Z]{3}\\d{4}$");
        Matcher matcher = pattern.matcher(placa);
        return matcher.find();
    }

}
